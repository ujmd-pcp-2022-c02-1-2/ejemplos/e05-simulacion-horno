﻿namespace Simulador_de_Horno
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.panelTermómetro = new System.Windows.Forms.Panel();
            this.panelMercurio = new System.Windows.Forms.Panel();
            this.panelEscala = new System.Windows.Forms.Panel();
            this.pbxHornoON = new System.Windows.Forms.PictureBox();
            this.pbxHornoOFF = new System.Windows.Forms.PictureBox();
            this.btnON = new System.Windows.Forms.Button();
            this.btnOFF = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.nudTemperatura = new System.Windows.Forms.NumericUpDown();
            this.tmrON = new System.Windows.Forms.Timer(this.components);
            this.tmrOFF = new System.Windows.Forms.Timer(this.components);
            this.tmrEstado = new System.Windows.Forms.Timer(this.components);
            this.panelTermómetro.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHornoON)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHornoOFF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTemperatura)).BeginInit();
            this.SuspendLayout();
            // 
            // panelTermómetro
            // 
            this.panelTermómetro.Controls.Add(this.panelMercurio);
            this.panelTermómetro.Location = new System.Drawing.Point(267, 58);
            this.panelTermómetro.Name = "panelTermómetro";
            this.panelTermómetro.Size = new System.Drawing.Size(14, 252);
            this.panelTermómetro.TabIndex = 1;
            // 
            // panelMercurio
            // 
            this.panelMercurio.BackColor = System.Drawing.Color.Red;
            this.panelMercurio.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelMercurio.Location = new System.Drawing.Point(0, 229);
            this.panelMercurio.Name = "panelMercurio";
            this.panelMercurio.Size = new System.Drawing.Size(14, 23);
            this.panelMercurio.TabIndex = 2;
            // 
            // panelEscala
            // 
            this.panelEscala.BackgroundImage = global::Simulador_de_Horno.Properties.Resources.Termómetro_26_180_Celcius;
            this.panelEscala.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panelEscala.Location = new System.Drawing.Point(245, 12);
            this.panelEscala.Name = "panelEscala";
            this.panelEscala.Size = new System.Drawing.Size(74, 367);
            this.panelEscala.TabIndex = 2;
            // 
            // pbxHornoON
            // 
            this.pbxHornoON.Image = global::Simulador_de_Horno.Properties.Resources.Alto_horno_encendido_GIF;
            this.pbxHornoON.Location = new System.Drawing.Point(47, 73);
            this.pbxHornoON.Name = "pbxHornoON";
            this.pbxHornoON.Size = new System.Drawing.Size(150, 150);
            this.pbxHornoON.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbxHornoON.TabIndex = 0;
            this.pbxHornoON.TabStop = false;
            this.pbxHornoON.Visible = false;
            // 
            // pbxHornoOFF
            // 
            this.pbxHornoOFF.Image = global::Simulador_de_Horno.Properties.Resources.Alto_horno_apagado;
            this.pbxHornoOFF.Location = new System.Drawing.Point(47, 73);
            this.pbxHornoOFF.Name = "pbxHornoOFF";
            this.pbxHornoOFF.Size = new System.Drawing.Size(150, 150);
            this.pbxHornoOFF.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbxHornoOFF.TabIndex = 0;
            this.pbxHornoOFF.TabStop = false;
            // 
            // btnON
            // 
            this.btnON.Location = new System.Drawing.Point(41, 242);
            this.btnON.Name = "btnON";
            this.btnON.Size = new System.Drawing.Size(75, 23);
            this.btnON.TabIndex = 3;
            this.btnON.Text = "Encender";
            this.btnON.UseVisualStyleBackColor = true;
            this.btnON.Click += new System.EventHandler(this.btnON_Click);
            // 
            // btnOFF
            // 
            this.btnOFF.Location = new System.Drawing.Point(122, 242);
            this.btnOFF.Name = "btnOFF";
            this.btnOFF.Size = new System.Drawing.Size(75, 23);
            this.btnOFF.TabIndex = 4;
            this.btnOFF.Text = "Apagar";
            this.btnOFF.UseVisualStyleBackColor = true;
            this.btnOFF.Click += new System.EventHandler(this.btnOFF_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 287);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Temperatura (°C):";
            // 
            // nudTemperatura
            // 
            this.nudTemperatura.Location = new System.Drawing.Point(133, 283);
            this.nudTemperatura.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.nudTemperatura.Minimum = new decimal(new int[] {
            26,
            0,
            0,
            0});
            this.nudTemperatura.Name = "nudTemperatura";
            this.nudTemperatura.Size = new System.Drawing.Size(67, 20);
            this.nudTemperatura.TabIndex = 6;
            this.nudTemperatura.Value = new decimal(new int[] {
            26,
            0,
            0,
            0});
            this.nudTemperatura.ValueChanged += new System.EventHandler(this.nudTemperatura_ValueChanged);
            // 
            // tmrON
            // 
            this.tmrON.Tick += new System.EventHandler(this.tmrON_Tick);
            // 
            // tmrOFF
            // 
            this.tmrOFF.Enabled = true;
            this.tmrOFF.Tick += new System.EventHandler(this.tmrOFF_Tick);
            // 
            // tmrEstado
            // 
            this.tmrEstado.Enabled = true;
            this.tmrEstado.Tick += new System.EventHandler(this.tmrEstado_Tick);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(343, 392);
            this.Controls.Add(this.nudTemperatura);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnOFF);
            this.Controls.Add(this.btnON);
            this.Controls.Add(this.panelTermómetro);
            this.Controls.Add(this.panelEscala);
            this.Controls.Add(this.pbxHornoON);
            this.Controls.Add(this.pbxHornoOFF);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.Text = "Simulación de Horno";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panelTermómetro.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxHornoON)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHornoOFF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTemperatura)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbxHornoOFF;
        private System.Windows.Forms.PictureBox pbxHornoON;
        private System.Windows.Forms.Panel panelTermómetro;
        private System.Windows.Forms.Panel panelMercurio;
        private System.Windows.Forms.Panel panelEscala;
        private System.Windows.Forms.Button btnON;
        private System.Windows.Forms.Button btnOFF;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudTemperatura;
        private System.Windows.Forms.Timer tmrON;
        private System.Windows.Forms.Timer tmrOFF;
        private System.Windows.Forms.Timer tmrEstado;
    }
}

