﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simulador_de_Horno
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void btnON_Click(object sender, EventArgs e)
        {
            tmrON.Start();
            tmrOFF.Stop();
        }

        private void btnOFF_Click(object sender, EventArgs e)
        {
            tmrOFF.Start();
            tmrON.Stop();
        }

        private void tmrEstado_Tick(object sender, EventArgs e)
        {
            if (tmrON.Enabled)
            {
                pbxHornoON.Visible = true;
            }
            else
            {
                pbxHornoON.Visible = false;
            }

            if (tmrOFF.Enabled)
            {
                pbxHornoOFF.Visible = true;
            }
            else
            {
                pbxHornoOFF.Visible = false;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            panelMercurio.Height = 0;
        }

        private void nudTemperatura_ValueChanged(object sender, EventArgs e)
        {
            panelMercurio.Height = ((int)nudTemperatura.Value - 26) *
                panelTermómetro.Height / (180 - 26);
        }

        private void tmrON_Tick(object sender, EventArgs e)
        {
            if (nudTemperatura.Value < 180)
            {
                nudTemperatura.Value++;
            }
        }

        private void tmrOFF_Tick(object sender, EventArgs e)
        {
            if (nudTemperatura.Value > 26)
            {
                nudTemperatura.Value--;
            }
        }
    }
}
