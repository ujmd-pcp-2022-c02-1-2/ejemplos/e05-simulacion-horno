# E05 Simulación Horno

Programa en Visual Studio que permita simular el encendido y apagado de un horno, reflejando así el incremento o decremento de temperatura visualmente.
El valor de la temperatura se podrá apreciar en grados Celcius gracias a una imagen con escala graduada.
